import unittest
from Instrument import *


class RandomDealDataTest(unittest.TestCase):
    def test_calculateNextPrice_checkCorrectDataType(self):
        instrument = Instrument(9971.0, -8.81, -10.066, 'Galactia')
        print(instrument)
        price = instrument.calculateNextPrice('S')
        assert isinstance(price, float), 'Incorrect data type' 

    def test_calculateNextPrice_checkIncorrectDataType():
        instrument = Instrument('abc', 'gfhh', -10.066, 'Galactia')
        price = instrument.calculateNextPrice('S')
        assert isinstance(price, int), 'Incorrect data type'

    def test_calculateNextPrice_withNotEnoughParams():
        instrument = Instrument(9971.0)
        price = instrument.calculateNextPrice('S')
        assert isinstance(price, float), 'Incorrect data type'

    def test_calculateNextPrice_withTooManyParams():
        instrument = Instrument(9971.0, -8.81, -10.066, 'Galactia','ac')
        price = instrument.calculateNextPrice('S')
        assert isinstance(price, float), 'Incorrect data type'

    def test_calculateNextPrice_withWrongOrder():
        instrument = Instrument('Galactia', 9971.0, -8.81, -10.066)
        price = instrument.calculateNextPrice('S')
        assert isinstance(price, float), 'Incorrect data type'
    
if __name__ == '__main__':
    unittest.main()
    